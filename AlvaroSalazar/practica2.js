function llenar() {
    var alumno = {};
    alumno.nombre = document.querySelector('#nombre').value;
    alumno.apellido = document.querySelector('#apellido').value;
    alumno.curso = document.querySelector('#curso').value;

    return alumno;
    }

function imprimir() {
    var estudianteLleno = llenar();
    console.log(estudianteLleno);
}

function imprimirTabla(){
    var estudianteLleno= llenar();
    const fila= document.createElement("tr");
    const columna= document.createElement("td");
    const columna1= document.createElement("td");
    const columna2= document.createElement("td");

    const nombre= document.createTextNode(estudianteLleno.nombre);
    const apellido= document.createTextNode(estudianteLleno.apellido);
    const curso= document.createTextNode(estudianteLleno.curso);

    columna.appendChild(nombre);
    columna1.appendChild(apellido);
    columna2.appendChild(curso);

    fila.appendChild(columna);
    fila.appendChild(columna1);
    fila.appendChild(columna2)  
    document.getElementById('tabla').append(fila);
}


